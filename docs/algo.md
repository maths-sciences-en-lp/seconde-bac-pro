---
author: Thomas Mounier
title: Algo / Python
---

Cette page a pour objectif de recenser des ressources concernant l'utilisation de Python. Elle est en construction.

<br>

Pour travailler en Python, vous n'avez pas besoin d'installer de logiciels. 
<br>


1. Méthode 1 : Je vous donne un travail à faire sur [Capytale](https://capytale2.ac-paris.fr/web/c-auth/list) et vous vous connectez à l'aide de vos identifiants EduConnect et du code fourni.
2. Méthode 2 : Vous voulez travailler en mode "bac à sable", alors je vous recommande d'aller sur [Basthon Python](https://console.basthon.fr/)

Dans l'année, nous allons réaliser quelques TP en Python. Vous n'avez pas besoin d'être des experts, mais il faudra essayer de faire preuve de bon sens et de logique


TP 1 : Probabilités [(accès direct)](https://capytale2.ac-paris.fr/web/c/7fe0-1726323) 

[Document support](https://forge.apps.education.fr/maths-sciences-en-lp/seconde-bac-pro/-/blob/main/docs/latex/Maths/0%20-%20Python/S1%20Proba.pdf?ref_type=heads)

??? note pliée "Cliquer pour afficher / cacher"
	<div class="centre">
	<iframe 
	    src="../latex/Maths/0%20-%20Python/S1%20Proba.pdf#toolbar=0"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



