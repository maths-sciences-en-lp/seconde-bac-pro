---
author: Thomas Mounier
title: Physique Chimie
---

2024 / 2025 : je n'enseigne pas la Physique Chimie en seconde, les ressources ici seront limitées !

## Méthodologie

- [Démarche scientifique](https://forge.apps.education.fr/maths-sciences-en-lp/seconde-bac-pro/-/blob/main/docs/latex/Sciences/D%C3%A9marche%20scientifique/2023%20demarche%20scientifique.pdf?ref_type=heads)
- [Rédiger un Compte Rendu de TP](https://forge.apps.education.fr/maths-sciences-en-lp/seconde-bac-pro/-/blob/main/docs/latex/Sciences/D%C3%A9marche%20scientifique/2024%20faire%20un%20CR%20de%20tp.pdf?ref_type=heads)
- [Application sur un premier TP](https://forge.apps.education.fr/maths-sciences-en-lp/seconde-bac-pro/-/blob/main/docs/latex/Sciences/D%C3%A9marche%20scientifique/2023%20premier%20tp%20thermique.pdf?ref_type=heads)


