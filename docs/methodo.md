---
author: Thomas Mounier
title: Méthodes
---

Des ressources sur les méthodes pour apprendre/travailler les mathématiques en LP en sortant de collège.

[En construction]

- [Des Flash Card sur les verbes de Consignes](https://forge.apps.education.fr/maths-sciences-en-lp/seconde-bac-pro/-/blob/main/docs/latex/Maths/23%2024/Maths%20divers/2nd%20brise%20glace%202%202023.pdf?ref_type=heads)


