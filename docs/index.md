---
hide:
- footer
---

# Bienvenue


    
</style>

![Thomas Mounier, PLP Maths Sciences](https://forge.apps.education.fr/tmounier/maths-sciences/-/raw/main/avatar2.jpg ){ width=15%; align=left } 

Bienvenue ! Ce site est principalement à destination de mes élèves. 


Ces derniers pourront y retrouver les documents utilisés pendant les cours (les pages seront visibles au fur et à mesure dans l'année, il peut aussi servir de "cahier de texte" approximatifs !)



Bonne visite !



<br>
<br>

Si vous êtes enseignant, vous y trouverez les documents (ainsi que les sources en LaTeX de ces derniers) en partage libre.
<br>


Ce site est hébergé sur [La Forge](https://forge.apps.education.fr/){:target="_blank"} des communs numériques éducatifs et créé via [un modèle](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review//#les-sites-modeles){:target="blank"}. 



<p xmlns:cc="http://creativecommons.org/ns#" >Les ressources sont déposées - quand elles sont de ma propre création - sous licence <a href="https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1" alt=""></a></p> 
