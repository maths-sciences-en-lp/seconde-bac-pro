---
author: Thomas Mounier
title: A propos de ce site
---

Le site est hébergé par la forge de [Apps Education](https://forge.apps.education.fr/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Les ressources sont disponibles librement sous la licence CC0 - dans une utopie de diffuser le savoir de manière libre !



