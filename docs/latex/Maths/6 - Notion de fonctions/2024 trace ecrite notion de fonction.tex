\documentclass[12pt,a4paper,oneside,dvipsnames,table,svgnames,skins,theorems]{report}
\usepackage{ProfCollege}
\usepackage{tmbasev3} %pour le moment placé dans la racine du .tex ... !
\usepackage{postit}


\DeclareSIPrefix\micro{\ensuremath{\mu}}{-6}

\matierechap{m} %m pour maths, pc pour physique chimie, rien pour vierge
\version{1} %1 : PROF   2: ELEVE sans corrigé  3: corrigé des exos sans le cours
\setcounter{chapter}{5} %compteur de chapitre

\graphicspath{{/home/thomas/Documents/LATEX nas/Forge/seconde-bac-pro/docs/latex/Images}} %chemin du fichier image


\newcolumntype{C}{>{\centering\arraybackslash}X}
\renewcommand{\tabularxcolumn}[1]{>{\arraybackslash}m{#1}}
\begin{document}

\ifsectioncours

\chapter{Notion de fonction}
\setcounter{page}{1}
\section{Définition et notations}
\begin{minipage}{.6\textwidth}%
En mathématiques une \textbf{fonction} est un objet qui transforme (via un ou plusieurs calculs) un nombre en un autre nombre. On peut représenter la situation par le schéma : 
\end{minipage}
\hfill
\begin{minipage}{.30\textwidth}%
\includegraphics[scale=0.85, width=\textwidth]{fonction1.png}
\end{minipage}
\vspace{0.2cm}

Le mécanisme à l'intérieur est différent pour chaque fonction.

\vspace{0.2cm}

\begin{tcolorbox}[arc=1ex,  colframe=orange, colback=orange!5!white]


      \textbf{Quelques notations importantes :} 
      \begin{itemize}
	\item[-] Une fonction se note par une lettre. Exemple : la fonction $\mathbf{f}$ 
	\item[-] Le nombre qui entre dans la fonction se note $\mathbf{x}$
	\item[-] Le nombre qui sort de la fonction après transformation se note $\mathbf{f(x)}$ et se nomme \textit{ image de $\mathbf{x}$ par la fonction $\mathbf{f}$}
	\item[-] On dit aussi que $\mathbf{x}$ est un antécédent de $\mathbf{f(x)}$ par la fonction $\mathbf{f}$
\end{itemize}

\end{tcolorbox}

\vspace{0.2cm}

\cadrep{Intérêt pratique}{
De manière générale une fonction relie deux grandeurs : le poids en fonction de la masse, le nombre de personnes dans un endroit en fonction de l'heure de la journée ...}

\vspace{0.2cm}

\textbf{Un exemple} : La fonction $f$ prend un nombre et le transforme en le multipliant par 3 et en ajoutant ensuite 2.
\vspace{0.1cm}

On prend quelques nombres : \\
Si je choisi le nombre 0, j'obtiens $ f(0)= 0 \times 3 +2 = 2$ : \textbf{l'image du nombre 0 par la fonction $f$ est 2.} \\
Si je choisi le nombre 5, j'obtiens $ f(5) = 5 \times 3 + 2 = 17$\\
Si je choisi le nombre -2 j'obtiens $ f(-2) =-2 \times 3 + 2 = -4$\\
Que l'on peut résumer dans un tableau : 
\vspace{0.5cm}

\begin{table}[h]
\centering

\resizebox{.3\textwidth}{!}{% <------ Don't forget this %
  \begin{tabular}{|c|c|c|c|}
  \hline
  $x$ & $-2$ & $0$ & $5$ \\
  \hline
  $f(x)$ & $-4$ & $2$ & $17$ \\
  \hline
  \end{tabular}% <------ Don't forget this %
  
}
\end{table}
\newpage

\section{Différentes formes de représentation}

Il faut être capable de jongler entre les différentes formes de représentation des fonctions que nous allons découvrir avec un exemple guidé :\\
\textcolor{blue}{La fonction $f$ prend un nombre, l'élève au carré et enlève 3} $$ f(x) = x^2 - 3$$

Cette écriture se nomme \fcolorbox{orange}{orange!10}{\textbf{forme algébrique}} de la fonction $f$. Elle permet de calculer, connaissant le nombre de départ, le nombre résultant par la fonction. \\

Exemple : $f(1) = 1 ^2 - 3 = -2 $ (on remplace \textit{x} par $1$)
\vspace{0.5\baselineskip}
Elle permet de passer au \fcolorbox{orange}{orange!10}{\textbf{tableau de valeurs}} de la fonction : \\
\begin{table}[h]
\centering

\resizebox{.5\textwidth}{!}{% <------ Don't forget this %
  \begin{tabular}{|c|c|c|c|c|c|c|c|}
  \hline
  $x$ & $-3$ & $-2$ & $-1$ & $0$ & $1$ & $2$ & $3$  \\
  \hline
  $f(x)$ & $6$ & $1$ & $-2$ & $-3$ & $-2$ & $1$ & $6$ \\
  \hline
  \end{tabular}% <------ Don't forget this %
  
}
\end{table}\\

Ce qui permet de tracer la \fcolorbox{orange}{orange!10}{\textbf{représentation graphique}} de la fonction :
\vspace{0.5\baselineskip}

\begin{center}

\begin{tikzpicture}[scale=0.8]
\tkzInit[xmin=-4,xmax=4,ymin=-3,ymax=4]
\tkzGrid[color=gray!30]
\tkzAxeXY
\tkzFct[color=blue]{x**2-3}
\tkzDefPointByFct(1)
\tkzText[above, right=0.2cm,text=blue](tkzPointResult){${\mathcal{C}}_f$}



\end{tikzpicture}
\end{center}

La courbe passe par les points du tableau et est tracée à main levée (surtout pas à la règle). On remarque que ce n'est pas une droite et qu'il y a deux parties : une partie "descendante" et une partie "ascendante" : approche du \fcolorbox{orange}{orange!10}{\textbf{tableau de variations}}.
\begin{center}
\resizebox{.8\textwidth}{!}{%
\begin{tikzpicture}
	\tkzTabInit[lgt=2.5, espcl=4, deltacl=0.75]{$x$ / 1 , variations de $f$ / 2}{$-3$,$\phantom{-}0$, $3$}
	
	\tkzTabVar{ +/$\phantom{-}6$, -/ $-3$, +/$6$ }
\end{tikzpicture}%
}
\end{center}
\newpage

\textbf{Règles de construction du tableau de variation :}\\
\begin{enumerate}
\item Sur la première ligne on indique à gauche le nombre minimum pour lequel on étudie la fonction.
\item Sur la première ligne à droite on indique à droite le nombre maximum pour lequel on étudie la fonction.\\
\end{enumerate}

\begin{center}
\resizebox{.8\textwidth}{!}{%
\begin{tikzpicture}
	\tkzTabInit[lgt=2.5, espcl=4, deltacl=0.75]{\textcolor{red}{$x$} / 1}{\textcolor{red}{$-3$},\textcolor{blue}{$3$}}
\end{tikzpicture}%
}
\end{center}

\begin{enumerate}
\item[3.] Sur la deuxième ligne on rajoute les variations de la fonction\\
\end{enumerate}

\begin{center}
\resizebox{.8\textwidth}{!}{%
\begin{tikzpicture}
	\tkzTabInit[lgt=2.5, espcl=4, deltacl=0.75]{\textcolor{red}{$x$} / 1 , variations de $f$ / 2}{\textcolor{red}{$-3$}, \textcolor{blue}{$3$}}
\end{tikzpicture}%
}
\end{center}
\begin{enumerate}
\item[4.] A partir de la courbe, on regarde sur l'intervalle étudié (ici $[-3;3]$) s'il y a un changement de variation (par exemple une courbe qui monte et qui descend ensuite). Si c'est le cas, on écrit dans le tableau la valeur de $x$ pour laquelle ce changement se produit.
\end{enumerate}

\begin{center}
\resizebox{.8\textwidth}{!}{%
\begin{tikzpicture}
	\tkzTabInit[lgt=2.5, espcl=4, deltacl=0.75]{\textcolor{red}{$x$} / 1, variations de $f$ / 2}{\textcolor{red}{$-3$},$\phantom{-}0$,\textcolor{blue}{$3$}}

\end{tikzpicture}%
}
\end{center}

\begin{enumerate}
\item[5.] On trace les flèches dans le tableau.
\item[6.] A l'extrémité de chaque flèche on rajoute le résultat de la transformation du nombre de la ligne des $x$ par la fonction. (lecture graphique ou calcul)
\end{enumerate}
\begin{center}
\resizebox{.8\textwidth}{!}{%
\begin{tikzpicture}
	\tkzTabInit[lgt=2.5, espcl=4, deltacl=0.75]{\textcolor{red}{$x$} / 1, variations de $f$ / 2}{\textcolor{red}{$-3$},$\phantom{-}0$,\textcolor{blue}{$3$}}
	\tkzTabVar{ +/$\phantom{-}6$, -/ $-3$, +/$6$ }
\end{tikzpicture}%
}
\end{center}

\fi
\ifsectionexos


\newpage
\section{Exercices}
\Opensolutionfile{mycor}[ficcorex]
\exo{}
\textbf{Traduire} par une expression algébrique les fonctions suivantes : \COMP{2}
\begin{enumerate}
\item La fonction $f$ prend le nombre de départ, le multiplie par $2$ et enlève $4$.
\item La fonction $g$ prend le nombre de départ, enlève $2$ et multiplie le total par $-3$.
\item La fonction $h$ prend le carré d'un nombre et ajoute $1$.
\item La fonction $i$ transforme un nombre en prenant sa racine carrée.
\end{enumerate}
\begin{correction}
\begin{enumerate}
\item $f(x)=2x-4$
\item $g(x)=(x-2) \times -3$
\item $h(x)=x^2+1$
\item $i(x)=\sqrt{x}$
\end{enumerate}
\end{correction}
\finexo

\exo{}
\textbf{Traduire} par une phrase les fonctions suivantes : \COMP{2}
\begin{enumerate}
\item $f(x)=x^3+2$
\item $g(x)=-(x-2) +3$
\item $h(x)=4x^2+3x-2$
\item $i(x)=3x+2$

\end{enumerate}
\begin{correction}
\begin{enumerate}
\item La fonction $f$ prend le nombre de départ, l'élève au cube et ajoute $2$.
\item La fonction $g$ prend le nombre de départ, l'élève au carré, prend son opposé et ajoute 3.
\item La fonction $h$ prend le nombre de départ, l'élève au carré, multiplie le résultat par $4$, ajoute $3$ fois le nombre de départ au résultat et enlève $2$.
\item La fonction $i$ prend le triple du nombre de départ et ajoute $2$.
\end{enumerate}
\end{correction}
\finexo

\exo{}
Soient deux fonctions $f$ et $g$ telles que $f(x)=x^2 +2$ et $g(x)=-3x$.
En détaillant les calculs, \textbf{compléter} le tableau suivant : \COMP{3}
\begin{table}[h]
\centering
\resizebox{.4\textwidth}{!}{% <------ Don't forget this %
  \begin{tabular}{|c|c|c|c|c|}
  \hline
  $x$ & $-2\phantom{0}$ & $\phantom{-}0\phantom{0}$ & $\phantom{-}3\phantom{0}$ & $\phantom{-}10 \phantom{0}$ \\
  \hline
  $f(x)$ & $ $ & $ $ & $ $ & $ $  \\
  \hline
  $g(x)$ & $ $ & $ $ & $ $ & $ $ \\
  \hline
  \end{tabular}% <------ Don't forget this %
}
\end{table}
\begin{correction}
Les calculs sont à rédiger sur feuille
\begin{table}[h]
\centering
\resizebox{.4\textwidth}{!}{% <------ Don't forget this %
  \begin{tabular}{|c|c|c|c|c|}
  \hline
  $x$ & $-2\phantom{0}$ & $\phantom{-}0\phantom{0}$ & $\phantom{-}3\phantom{0}$ & $\phantom{-}10 \phantom{0}$ \\
  \hline
  $f(x)$ & $ 6 $ & $ 2 $ & $ 11 $ & $ 102 $  \\
  \hline
  $g(x)$ & $6 $ & $ 0 $ & $ -9 $ & $ -30 $ \\
  \hline
  \end{tabular}% <------ Don't forget this %
}
\end{table}
\end{correction}

\finexo

\exo{}
On donne trois courbes qui représentent trois fonctions $f$ , $g$ , $h$ et on sait  :
\begin{itemize}
\item[*]La courbe représentative de $f$ passe par le point $A(-3;2)$
\item[*]La courbe représentative de $g$ coupe l'axe des ordonnées au point d'ordonnée $-3$
\item[*]L'ordonnée du point d'abscisse $3$ est de $1$ pour la courbe de la fonction $h$
\end{itemize}
\textbf{Associer} à chaque courbe la fonction correspondante. \COMP{2}
\begin{center}
\resizebox{.4\textwidth}{!}{%
\begin{tikzpicture}

\coordinate (O) at (0,0);
\draw[very thin,gray]  (-5,-6)grid(5,7);
\draw[->, -latex] (-5,0)--(5.4,0) node[right]{$x$};
\foreach \x in {-5,...,-1}{
	\draw (\x,0.1cm) -- (\x,-0.1cm) node[below] {$\x\phantom{-}\strut$};
	}
\foreach \x in {1,...,5}{
	\draw (\x,0.1cm) -- (\x,-0.1cm) node[below] {$\x\strut$};
	}	
\draw[->, -latex] (0,-6)--(0,7.4) node[above]{$y$};;
\foreach \y in {-6,...,7}{
	\draw (0.1cm,\y) -- (-0.1cm,\y) node[left] {$\y\strut$};
	}
\draw [domain=-5:5] [samples=200] [color=blue] [line width = 1.6 pt] plot(\x,{(\x)^3*-0.04+(\x)^2*0.02+\x*0.09+1});
%\node[text=blue, thick] at (-3.5,5){$C_f$};
\draw [domain=2:5] [samples=500] [color=red] [line width = 1.6 pt] plot(\x,{1.11*(\x)^4-14.56*(\x)^3+66.89*(\x)^2-123.11*\x+71.3});
\draw [domain=-3:1] [samples=200] [color=orange] [line width = 1.6 pt] plot(\x,{3*(\x)^2+6*\x-3});
\end{tikzpicture}%
}
\begin{correction}
La courbe qui coupe les ordonnées à -3 est la parabole en orange, c'est donc la fonction $g$. La courbe qui passe par le point A est la bleue (celle qui commence la plus à gauche), c'est la fonction $f$. La dernière est donc la fonction $h$, cela se vérifie grâce au point $B(3;1)$.

\end{correction}

\end{center}
\finexo
\newpage
\exo{}
Pour chacune des 4 fonctions tracées sur le graphique, \textbf{dire} si elles sont : croissantes - décroissantes - constantes.  \COMP{2}
\begin{center}
\resizebox{.6\textwidth}{!}{%
\begin{tikzpicture}

\coordinate (O) at (0,0);
\draw[very thin,gray]  (-5,-6)grid(5,7);
\draw[->, -latex] (-5,0)--(5.4,0) node[right]{$x$};
\foreach \x in {-5,...,-1}{
	\draw (\x,0.1cm) -- (\x,-0.1cm) node[below] {$\x\phantom{-}\strut$};
	}
\foreach \x in {1,...,5}{
	\draw (\x,0.1cm) -- (\x,-0.1cm) node[below] {$\x\strut$};
	}	
\draw[->, -latex] (0,-6)--(0,7.4) node[above]{$y$};;
\foreach \y in {-6,...,7}{
	\draw (0.1cm,\y) -- (-0.1cm,\y) node[left] {$\y\strut$};
	}
\draw [domain=-5:5] [samples=200] [color=blue] [line width = 1.6 pt] plot(\x,{3});
\node[text=blue, thick] at (-1,3.5){$C_f$};
\draw [domain=-1.5:2.5] [samples=200] [color=red] [line width = 1.6 pt] plot(\x,{-3*\x+2});
\node[text=red, thick] at (2.2,-3.5){$C_g$};

\draw [domain=-2:1] [samples=200] [color=orange] [line width = 1.6 pt] plot(\x,{3*(\x)^2+2*\x-1});
\node[text=orange] at (1.4,4){$C_h$};
\draw [domain=0.2:4] [samples=200] [color=green] [line width = 1.6 pt] plot(\x,{1/ \x});
\node[text=green] at (4.3,0.5){$C_i$};


\end{tikzpicture}%
}
\end{center}

\begin{correction}
La courbe $C_f$ est constante; la courbe $C_h$ est d'abord décroissante puis croissante, la courbe $C_g$ est décroissante et la courbe $C_i$ aussi.
\end{correction}
\finexo

\exo{}
Soit la fonction $f(x)=x^2+3$ dont la courbe est représentée après.
\textbf{Dresser} le tableau de variation de la fonction sur $[-2;2]$. \COMP{3}
\begin{center}
\resizebox{.35\textwidth}{!}{%
\begin{tikzpicture}

\coordinate (O) at (0,0);
\draw[very thin,gray]  (-4,-2)grid(4,10);
\draw[->, -latex] (-4,0)--(4.4,0) node[right]{$x$};
\foreach \x in {-4,...,-1}{
	\draw (\x,0.1cm) -- (\x,-0.1cm) node[below] {$\x\phantom{-}\strut$};
	}
\foreach \x in {1,...,4}{
	\draw (\x,0.1cm) -- (\x,-0.1cm) node[below] {$\x\strut$};
	}	
\draw[->, -latex] (0,-2)--(0,10) node[above]{$y$};;
\foreach \y in {-2,...,10}{
	\draw (0.1cm,\y) -- (-0.1cm,\y) node[left] {$\y\strut$};
	}
\draw [domain=-2.5:2.5] [samples=200] [color=blue] [line width = 1.6 pt] plot(\x,{(\x)^2+3});
\node[text=blue, thick] at (-1.5,4){$C_f$};



\end{tikzpicture}%
}
\end{center}
\begin{correction}
Tableau de variations :
\begin{center}
\resizebox{.6\textwidth}{!}{%
\begin{tikzpicture}
	\tkzTabInit[lgt=2.5, espcl=4, deltacl=0.75]{$x$ / 1 , variations de $f$ / 2}{$-2$,$\phantom{-}0$, $2$}
	
	\tkzTabVar{ +/$\phantom{-}7$, -/ $\phantom{-}3$, +/$7$ }
\end{tikzpicture}%
}
\end{center}
\end{correction}
\finexo

\exo{}
On donne le graphique suivant qui représente la concentration dans le sang de paracétamol suivant la forme prise (en cachet en bleu, effervescent en rouge).\\
Plus la concentration est forte, plus le médicament agit.
\begin{center}


\begin{minipage}{.60\textwidth}%
\includegraphics[scale=0.85]{fonction2.png}
\end{minipage}\\
\end{center}
\begin{enumerate}

\item Pour chaque forme du médicament \textbf{donner} la concentration au bout de \SI{30}{\minute}. \COMP{1}
\item Préciser en combien de temps, pour chaque forme, la dose maximale est atteinte. \COMP{1}
\item Pour chaque forme, \textbf{donner} la durée pendant laquelle la concentration est croissante. \COMP{2}
\item Préciser, pour chaque forme, le temps nécessaire pour atteindre une concentration de \SI[per-mode=symbol]{13}{\micro\gram\per\liter}. \COMP{2}
\item \textbf{Quelle forme choisiriez vous} si vous vouliez calmer une douleur le plus rapidement possible ? \COMP{4}
\end{enumerate}
\begin{correction}
1. Pour la courbe en rouge on est autour de 16 mg/L, pour la bleue autour de 10.\\
2. Pour la rouge le max est atteint en 40 min. Pour la bleue en 100 min.\\
3. Pour la forme en rouge l'intervalle de croissance est $[0;40]$ et pour l'autre $[0;100]$\\
4. La concentration de 13 mg/L est atteinte deux fois pour chaque courbe; admettons que nous cherchons la première occurrence (action croissante du médicament !) : en rouge atteint pour environ 20 min, en bleu atteinte pour environ 50 min.\\
5. Je choisirai la forme rouge qui atteint le pic plus vite que la bleue.
\end{correction}
\finexo

\exo{}
En France, le cannabis est interdit. Il est détecté, entre autres, par une analyse urinaire si l'on dépasse une concentration de \SI{50}{\micro\gram\per\liter}. On a modélisé la concentration de cannabis (THC) dans les urines d'une conductrice ayant consommé un seul joint.\\
La courbe suivante représente la fonction $f$, concentration en fonction du temps.
\begin{center}
\begin{minipage}{.60\textwidth}%
\includegraphics[scale=0.5]{fonction3.png}
\end{minipage}\\
\end{center}
\begin{enumerate}
\item \textbf{Chercher} la signification du symbole $\mu$ ? \COMP{1}
\item \textbf{Relever} sur le graphique la valeur maximale de la concentration en THC. \COMP{1}
\item \textbf{Préciser} au bout de combien de temps (heures) elle est atteinte. \COMP{2}
\item \textbf{Résoudre} graphiquement $f(x)=$\SI{50}{\micro\gram\per\liter}. \COMP{3}
\item \textbf{Déduire} le nombre de jours pendant lequel le cannabis pourra être détecté. \COMP{4}
\end{enumerate}
\begin{correction}
1. Ce symbole signifie "micro" $10^{-6}$.\\
2. 3. Le maximum est d'environ $\SI[per-mode=symbol]{550}{\micro\gram\per\liter}$ et atteint au bout de $\SI{30}{\hour}$\\
4. 5. Deux solution : au bout de $\SI{0}{\hour}$ et au bout de $\SI{160}{\hour}$. $\SI{160}{\hour}$ équivaut à presque $7$jours.

\end{correction}
\finexo
\fi
\ifsectioncorrige
\newpage
\begin{center}
Corrigé des exercices
\end{center}
\setcounter{page}{1}
\Closesolutionfile{mycor}
\Readsolutionfile{mycor}
\fi
\end{document}